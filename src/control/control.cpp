#include "control.h"
#include <iostream>

Controller::Controller(RobotModel &rmIn):
  model  (rmIn)
{
}

Eigen::Vector7d Controller::Dqd ( 
          const Eigen::Vector7d & q,
          const Eigen::Affine3d & xd,
          const Eigen::Vector6d & Dxd_ff
                      )
{  
  //TODO Compute joint velocities able to track the desired cartesian position
  model.FwdKin(X, J, q); 

  //Desired linear velocities
  dX_desired.block<3,1>(0,0) = (xd.translation()-X.translation())*kp + Dxd_ff.block<3,1>(0,0);

  //Desired angular velocities
  Eigen::Matrix3d xd_rot = xd.linear();
  Eigen::Matrix3d x_rot = X.linear();
  Eigen::AngleAxisd XRD(xd_rot*x_rot.transpose());
  dX_desired.block<3,1>(3,0) = XRD.angle()*XRD.axis()*kp + Dxd_ff.block<3,1>(3,0);

  //Joint velocities
  Eigen::Matrix <double, 6, 6> J_inv;
  J_inv = J*J.transpose();
  Eigen::Vector7d Dqd = J.transpose()*J_inv.completeOrthogonalDecomposition().pseudoInverse()*dX_desired;

  return Dqd;
}

