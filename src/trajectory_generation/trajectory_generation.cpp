#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
  pi = piIn;
  pf = pfIn;
  Dt = DtIn;
  a[0] = pi;
  a[1] = 0;
  a[2] = 0;
  a[3] = 10*(pf-pi);
  a[4] = 15*(pi-pf);
  a[5] = 6*(pf-pi);
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
  pi = piIn;
  pf = pfIn;
  Dt = DtIn;
  a[0] = pi;
  a[1] = 0;
  a[2] = 0;
  a[3] = 10*(pf-pi);
  a[4] = 15*(pi-pf);
  a[5] = 6*(pf-pi);
};

const double  Polynomial::p     (const double &t){
  //TODO compute position
  double p = a[0] + a[1]*(t/Dt) + a[2]*pow(t/Dt,2) + a[3]*pow(t/Dt,3) + a[4]*pow(t/Dt,4) + a[5]*pow(t/Dt,5);
  return p;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  double dp = a[1]/Dt + 2*a[2]*(t/pow(Dt,2)) + 3*a[3]*(pow(t,2)/pow(Dt,3)) + 4*a[4]*(pow(t,3)/pow(Dt,4)) + 5*a[5]*(pow(t,4)/pow(Dt,5));
  return dp;
};

Frame2Frame::Frame2Frame(const Eigen::Affine3d & xi, const Eigen::Affine3d & xf, const double & DtIn){

  polx.update(xi.translation().x(),xf.translation().x(),DtIn);  
  poly.update(xi.translation().y(),xf.translation().y(),DtIn);
  polz.update(xi.translation().z(),xf.translation().z(),DtIn);
  Ri = xi.linear();
  Rf = xf.linear();
  Eigen::Matrix3d Rif = Rf*Ri.transpose(); //Rotation matrix between initial and final
  Eigen::AngleAxisd Rif2(Rif); //Initialisation of angle axis matrix
  axis = Rif2.axis();
  pola.update(0,Rif2.angle(),DtIn); //Angle difference between the reference and the final
}

Eigen::Affine3d Frame2Frame::X(const double & time){ 

  Eigen::Affine3d X; //Initialization of the transformation matrix
  X.translation() = Eigen::Vector3d(polx.p(time),poly.p(time),polz.p(time)); //Translation part
  double angle = pola.p(time); //Final Angle part
  X.linear() = Eigen::AngleAxisd(angle,axis).matrix();  //Rotation part

  return X;
}

Eigen::Matrix<double,6,1> Frame2Frame::dX(const double & time){ 

  Eigen::Vector3d dPos (polx.dp(time),poly.dp(time),polz.dp(time)); //Cartesian velocity at a given time
  Eigen::Vector3d dAng (pola.dp(time)*axis); //Angular velocity
  Eigen::Matrix<double,6,1> dX;
  dX.head(3) = dPos;
  dX.tail(3) = dAng;
  
  return dX;
}