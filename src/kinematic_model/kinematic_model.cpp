#include "kinematic_model.h" 
#include <iostream>

RobotModel::RobotModel(const std::array<double,8> &aIn, const std::array<double,8> &dIn, const std::array<double,8> &alphaIn):
  a     (aIn), 
  d     (dIn),
  alpha (alphaIn)
{};

void RobotModel::FwdKin(Eigen::Affine3d &xOut, Eigen::Matrix67d &JOut, const Eigen::Vector7d & qIn){
  // TODO Implement the forward and differential kinematics
  Eigen::Vector7d q = qIn;
  Eigen::Matrix4d T;
  Eigen::VecMat ListT;
  
  double i;
  for(i=0;i<7;i+=1)
  {
    //Transformation matrix for each joint
    T << cos(q[i]),               -sin(q[i]),                   0,             a[i],
         cos(alpha[i])*sin(q[i]), cos(alpha[i])*cos(q[i]), -sin(alpha[i]), -d[i]*sin(alpha[i]),
         sin(alpha[i])*sin(q[i]), sin(alpha[i])*cos(q[i]),  cos(alpha[i]),  d[i]*cos(alpha[i]),
         0,                         0,                          0,              1;  
 
    ListT[i] = T;
  }
  ListT[7] = ListT[0];

  Eigen::VecVec Z;
  Z[0] = ListT[0].block<3,1>(0,2);

  for(i=1;i<7;i+=1)
  {
    //ListT has in order T01 - T02 - T03 - T04 - T05 - T06 - T07 - T07
    ListT[7] = ListT[7]*ListT[i];
    ListT[i] = ListT[7];
    //Z has in order Z1 - Z2 - Z3 - Z4 - Z5  - Z6 - Z7
    Z[i] = ListT[7].block<3,1>(0,2);
  }
    
  //xOut = T07 = ListT[7]  
  Eigen::Vector3d Tr = ListT[7].block<3,1>(0,3); 
  xOut.translation() = Tr; //Translation part
  Eigen::Matrix3d Rot = ListT[7].block<3,3>(0,0); 
  xOut.linear() = Rot; //Rotation part

  Eigen::VecVec P;

  for(i=0;i<7;i+=1)
  {
    //P has in order P17 - P27 - P37 - P47 - P57 - P67 - P77
    P[i] = ListT[7].block<3,1>(0,3) - ListT[i].block<3,1>(0,3);
  }

  Eigen::VecVec ZP;

  for(i=0;i<7;i+=1)
  {
    //C has in order C1 - C2 - C3 - C4 - C5 - C6 - C7
    ZP[i] = Z[i].cross(P[i]);
  }

  Eigen::Matrix67d J07;
  J07 << ZP[0], ZP[1], ZP[2], ZP[3], ZP[4], ZP[5], ZP[6], 
         Z[0],  Z[1],  Z[2],  Z[3],  Z[4],  Z[5],  Z[6];

  Eigen::Matrix3d I = Eigen::Matrix3d::Identity();
  Eigen::Matrix3d C = Eigen::Matrix3d::Identity();
  Eigen::Matrix3d O = Eigen::Matrix3d::Zero();
  Eigen::Matrix3d D;
  D << 0,                            a[6]*Rot(2,0)+d[7]*Rot(2,2), -a[6]*Rot(1,0)-d[7]*Rot(1,2),
       -a[6]*Rot(2,0)-d[7]*Rot(2,2), 0,                            a[6]*Rot(0,0)+d[7]*Rot(0,2),
       a[6]*Rot(1,0)+d[7]*Rot(1,2), -a[6]*Rot(0,0)-d[7]*Rot(0,2),  0;

  Eigen::Matrix <double,6,6> IDOC; 
  IDOC << I, D, 
          O, C;

  JOut = IDOC*J07;


}
