#include "control.h"
#include "trajectory_generation.h"
#include <iostream>

int main(){
  // Simulate the motion of a Panda robot using all three libraries
  const std::array<double,8> a {0,0,0,0.0825,-0.0825,0,0.088,0};
  const std::array<double,8> d {0.333,0,0.316,0,0.384,0,0,0.107};
  const std::array<double,8> alpha {0,-M_PI_2,M_PI_2,M_PI_2,-M_PI_2,M_PI_2,M_PI_2,0};
  Eigen::Vector6d Dxd_ff; //Joint velocity feedforward term
  Eigen::Matrix67d J; //Jacobian matrix
  Eigen::Affine3d Xd; //Desired cartesian position
  Eigen::Affine3d Xi = Eigen::Affine3d::Identity(); //Initial position
  Eigen::Affine3d Xc = Xi; //Current cartesian position
  Eigen::Affine3d Xf; //Final position

  //Initialisation of the classes
  RobotModel MyRobotModel(a,d,alpha);
  Controller MyController(MyRobotModel);
  
  Eigen::Vector7d q1(0,0,0,0,0,0,0);
  MyRobotModel.FwdKin(Xi,J,q1);
  Eigen::Vector7d q2(M_PI_2,M_PI_2,M_PI_2,M_PI_2,M_PI_2,M_PI_2,M_PI_2);
  MyRobotModel.FwdKin(Xf,J,q2);

  const double DtIn = 1; //Time interval
  Frame2Frame MyTrajectory(Xi,Xf,DtIn);

  float t;
  float dt = 0.001;

  std::cout <<"t, Xd_x, Xd_y, Xd_z, Xc_x, Xc_y, Xc_z" << "\n"; //Plot's variables
  for(t=0;t<DtIn;t=t+dt)
  {
    MyRobotModel.FwdKin(Xc,J,q1); // Forward kinematics
    Xd = MyTrajectory.X(t); // Desired position
    Dxd_ff = MyTrajectory.dX(t); // Joint velocity feedforward term
    Eigen::Vector7d Dqd = MyController.Dqd(q1,Xd,Dxd_ff); //Joint velocities

    std::cout << t << "," << Xd(0,3) << "," << Xd(1,3) << "," << Xd(2,3) << ","  << Xc(0,3) << "," << Xc(1,3) << "," << Xc(2,3) << "\n";
    q1 = Dqd*dt + q1; // New joint position
  }

}