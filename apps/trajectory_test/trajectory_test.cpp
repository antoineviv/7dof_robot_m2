#include "trajectory_generation.h"
#include <iostream>


int main(){
  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities

  const double piIn = 0; //Initial position
  const double pfIn = 1; //Final position
  const double DtIn = 1; //Time interval
  const double t = 0; //Specific time
  Eigen::Affine3d X_i {Eigen::Affine3d::Identity()};
  Eigen::Vector3d T_i (0,0,0); 
  Eigen::Matrix3d R_i {{1,0,0},{0,1,0},{0,0,1}}; 
  X_i.translation() = T_i;
  X_i.linear() = R_i;
  Eigen::Affine3d X_f {Eigen::Affine3d::Identity()};
  Eigen::Vector3d T_f (1,1,1); 
  Eigen::Matrix3d R_f {{1,0,0},{0,0,1},{0,-1,0}}; //Rotation around x
  X_f.translation() = T_f;
  X_f.linear() = R_f;

  Eigen::Affine3d X1; //Current frame
  Eigen::Affine3d X2; //Next frame
  Eigen::Matrix<double,6,1> dXa; //Analytical velocity
  Eigen::Matrix<double,6,1> dXn; //Numerical velocity


  //Initialisation of the classes
  Polynomial MyPolynomial(piIn, pfIn, DtIn);
  Frame2Frame MyTrajectory(X_i, X_f, DtIn);

  //std::cout << "t,dX,dY,dZ,wx,wy,wz \n"; // Datas for the plot juggler

  double dt;
  for(dt=0;dt<DtIn;dt+=0.1)
  {
    X1 = MyTrajectory.X(dt);
    X2 = MyTrajectory.X(dt+0.1);

    dXa = MyTrajectory.dX(dt);

    //std::cout << dt << "," << dXa(0) << "," << dXa(1) << "," << dXa(2) << "," << dXa(3) << "," << dXa(4) << "," << dXa(5) << "\n";


    std::cout << " At T = " << dt << "\n ";
    std::cout << " The velocity analytical is " << dXa << "\n ";
    //std::cout << " The velocity numerical is " << dXn << "\n ";

  }



}