#include "kinematic_model.h"
#include <iostream>

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  // For a small variation of q, compute the variation on X and check dx = J . dq  

  const std::array<double,8> aIn {0,0,0,0.0825,-0.0825,0,0.088,0};
  const std::array<double,8> dIn {0.333,0,0.316,0,0.384,0,0,0.107};
  const std::array<double,8> alphaIn {0,-M_PI_2,M_PI_2,M_PI_2,-M_PI_2,M_PI_2,M_PI_2,0};
  
  //Eigen::Vector7d qIn(M_PI_4/2, M_PI/7, M_PI/6, M_PI/5, M_PI_4, M_PI/3, M_PI_2);
  Eigen::Vector7d qIn(0,0,0,0,0,0,0);
  Eigen::Affine3d Xc =  Eigen::Affine3d::Identity();//Current frame
  Eigen::Matrix67d J; //Jacobian matrix

  RobotModel MyRobotModel = RobotModel(aIn, dIn, alphaIn);
  //MyRobotModel.FwdKin(Xc, J, qIn);

  std::cout << "i, x, y, z, Xx, Xy, Xz, Yx, Yy, Yz, Zx, Zy, Zz" << "\n"; //Plot's variables


  double i = 0;
  //Test for a rotation around z
  while(M_PI_2-i>0)
  {
    qIn << i,0,0,0,0,0,0;
    MyRobotModel.FwdKin(Xc, J, qIn);
    //std::cout << "The position is \n" << Xc.matrix() << "\n";
    std::cout << i << "," << Xc.matrix()(0,3) << "," << Xc.matrix()(1,3) << "," << Xc.matrix()(2,3) << ","  << Xc.matrix()(0,0) << "," << Xc.matrix()(1,0) << "," << Xc.matrix()(2,0) << "," << Xc.matrix()(0,1) << "," << Xc.matrix()(1,1) << "," << Xc.matrix()(2,1) << "," << Xc.matrix()(0,2) << "," << Xc.matrix()(1,2) << "," << Xc.matrix()(2,2) << "\n";

    i+=0.1;
  }

  //std::cout << "The position is \n" << Xc.translation() << "\n";
  //std::cout << "The rotation is \n" << Xc.linear() << "\n";
  //std::cout << "The Jacobian matrix is \n" << J << "\n";
  
}
